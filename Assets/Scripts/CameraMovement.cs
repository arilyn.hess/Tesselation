using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public static CameraMovement Instance;

    public Camera cam;

    public Transform cameraTarget;

    public float backset = 14f;

    private Vector3 offset;

    //private bool rotationLock = false;

    private int rotation = 0;
    public int Rotation
    {
        get => rotation;
        set
        {
            rotation = value;
            if (rotation > 3) rotation = 0;
            else if (rotation < 0) rotation = 3;
        }
    }

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(gameObject);

        offset = new Vector3(0f, 14f, -backset);
        cam = GetComponent<Camera>();
    }

    private void Update()
    {
        
        //move camera to target offset position.
        transform.position = new Vector3(cameraTarget.position.x + offset.x, cameraTarget.position.y + offset.y, cameraTarget.position.z + offset.z);

        //rotate left
        if (Input.GetKeyDown("q"))
        {
            //rotationLock = true;
            Vector3 targetRotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 90f, transform.eulerAngles.z);
            Rotation++;

            //snap rotation
            transform.eulerAngles = targetRotation;

            //update offset for new angle
            UpdateOffset();
        }
        //rotate right
        else if (Input.GetKeyDown("e"))
        {
            //rotationLock = true;
            Vector3 targetRotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - 90f, transform.eulerAngles.z);

            Rotation--;

            //snap rotation
            transform.eulerAngles = targetRotation;

            //update offset for new angle
            UpdateOffset();
        }

        //zoom in
        if (Input.GetKeyDown("up") && cam.fieldOfView > 10)
        {
            cam.fieldOfView = Camera.main.fieldOfView / 2;
        }
        //zoom out
        else if (Input.GetKeyDown("down") && cam.fieldOfView < 40)
        {
            cam.fieldOfView = Camera.main.fieldOfView * 2;
        }

    }

    //set new offset based on our current angle
    private void UpdateOffset()
    {
        
        float _targetOffsetX = offset.x;
        float _targetOffsetZ = offset.z;
        switch (Rotation)
        {
            case 1:
                _targetOffsetX = -backset;
                _targetOffsetZ = 0;

                break;
            case 2:
                _targetOffsetX = 0;
                _targetOffsetZ = backset;

                break;
            case 3:
                _targetOffsetX = backset;
                _targetOffsetZ = 0;

                break;
            default:
                _targetOffsetX = 0;
                _targetOffsetZ = -backset;
                break;
        }

        offset = new Vector3(_targetOffsetX, offset.y, _targetOffsetZ);
    }
}
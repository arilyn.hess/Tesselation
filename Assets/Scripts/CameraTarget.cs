using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public static CameraTarget Instance;

    public Transform following;

    private float moveSpeed = 4f;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        if (following) transform.position = following.position;

        //cancel inputs if we're not locked in combat
        if (!MovementController.Instance.combatLock)
        {
            following = MovementController.Instance.transform;
            return;
        }

        float xMove = 0;
        float yMove = 0;
        switch (CameraMovement.Instance.Rotation)
        {
            default:
                xMove = Input.GetAxisRaw("Horizontal");
                yMove = Input.GetAxisRaw("Vertical");
                break;
            case 1:
                xMove = Input.GetAxisRaw("Vertical");
                yMove = -Input.GetAxisRaw("Horizontal");
                break;
            case 2:
                xMove = -Input.GetAxisRaw("Horizontal");
                yMove = -Input.GetAxisRaw("Vertical");
                break;
            case 3:
                xMove = -Input.GetAxisRaw("Vertical");
                yMove = Input.GetAxisRaw("Horizontal");
                break;
        }

        //if control is moving
        if (Mathf.Abs(xMove) > 0 || Mathf.Abs(yMove) > 0)
        {
            //unlock follow
            following = null;

            //move camera
            transform.position = new Vector3(transform.position.x + xMove * Time.deltaTime * moveSpeed, transform.position.y, transform.position.z + yMove * Time.deltaTime * moveSpeed);
        }
    }
}

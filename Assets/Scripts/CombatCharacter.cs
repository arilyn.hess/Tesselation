using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatCharacter : MonoBehaviour
{
    public GridCharacter grid;

    public static CombatCharacter MovingCharacter;

    public float baseMoveRange = 4f;
    public float moveRange = 4f;

    private void Awake()
    {
        grid = GetComponent<GridCharacter>();
    }

    public void ActivateMovement()
    {
        MovingCharacter = this;
        StartCoroutine(grid.EnableWalkRange(moveRange));
    }
}

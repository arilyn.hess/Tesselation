using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CombatController
{ 
    public static void BeginCombat()
    {
        //lock free movement
        MovementController.Instance.combatLock = true;

        //find our current node and place player on it.
        NodeSystems _currentSystem = GridSystems.Instance.GetSystemsFromPoint(MovementController.Instance.transform.position.x, MovementController.Instance.transform.position.y * 4, MovementController.Instance.transform.position.z);

        var _character = MovementController.Instance.GetComponent<CombatCharacter>();
        var _agent = _character.GetComponent<NavMeshAgent>();
        _agent.ResetPath();
        if (_currentSystem)
        {
            _currentSystem.SetCharacter(_character.grid);
            _agent.SetDestination(_currentSystem.transform.position);

            _character.ActivateMovement();
        }

        NotificationController.Instance.StartCoroutine(NotificationController.Instance.DisplayNotification("BATTLE"));
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class CombatTrigger : MonoBehaviour
{
    public List<GameObject> units = new List<GameObject>();

    private void OnTriggerEnter(Collider collision)
    {
        //check if collision is player character
        if (collision.gameObject == MovementController.Instance.gameObject)
        {
            //it's the player

            CombatController.BeginCombat();

            Destroy(gameObject);
        }
    }
}

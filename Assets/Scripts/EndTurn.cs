using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurn : MonoBehaviour
{
    CombatCharacter character;

    private void Start()
    {
        character = MovementController.Instance.GetComponent<CombatCharacter>();
    }

    public void OnClick()
    {
        character.moveRange = character.baseMoveRange;

        character.ActivateMovement();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NodeParse
{
    public NodeSystems system;

    public NodeParse closestConnection;

    public float g;
    public float h;
    public float f => g + h;

    public NodeParse(NodeSystems _system, NodeParse _closestConnection, float _g, float _h)
    {
        system = _system;
        g = _g;
        h = _h;
        closestConnection = _closestConnection;
    }
}

public class GridCharacter : MonoBehaviour
{
    public NodeSystems currentNode;
    public CombatCharacter combat;

    public SpriteRenderer characterRenderer;

    //0 down, 1 left, 2 up, 3 downleft, 4 upleft; flip for 5, 6, 7
    public List<Sprite> idleSprites = new List<Sprite>();

    //0 south, 1 west, 2 north, 3 east, 4 southwest, 5 northwest, 6 northeast,  7 southeast
    public int currentDirection = 0;

    public float testRange = 5f;

    public Vector3 startCoordinate = new Vector3(0, 0, 0);

    private void Start()
    {
        GridSystems.Instance.gridNodes  [   
                                            Mathf.FloorToInt(startCoordinate.x), 
                                            Mathf.FloorToInt(startCoordinate.y),
                                            Mathf.FloorToInt(startCoordinate.z)
                                        ].SetCharacter(this);
        transform.position = currentNode.transform.position;
        characterRenderer = GetComponentInChildren<SpriteRenderer>();
        combat = GetComponent<CombatCharacter>();
    }

    private void Update()
    {
        //display range
        if (Input.GetKeyDown("f"))
        {
            StartCoroutine(EnableSphereRange(testRange));
        }

        transform.localScale = new Vector3(1, 1, 1);
        //show sprite based on our current direction and the camera's current rotation
        Debug.Log($"CurrentDirection: {currentDirection}");
        switch (CameraMovement.Instance.Rotation)
        {

            case 0:
                switch (currentDirection)
                {
                    case 0:
                        characterRenderer.sprite = idleSprites[0];
                        break;
                    case 1:
                        characterRenderer.sprite = idleSprites[1];
                        break;
                    case 2:
                        characterRenderer.sprite = idleSprites[2];
                        break;
                    case 3:
                        characterRenderer.sprite = idleSprites[1];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 4:
                        characterRenderer.sprite = idleSprites[3];
                        break;
                    case 5:
                        characterRenderer.sprite = idleSprites[4];
                        break;
                    case 6:
                        characterRenderer.sprite = idleSprites[4];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 7:
                        characterRenderer.sprite = idleSprites[3];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                }
                break;
            case 1:
                switch (currentDirection)
                {
                    //0 south, 1 west, 2 north, 3 east, 4 southwest, 5 northwest, 6 northeast,  7 southeast
                    case 0:
                        characterRenderer.sprite = idleSprites[1];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 1:
                        characterRenderer.sprite = idleSprites[0];
                        break;
                    case 2:
                        characterRenderer.sprite = idleSprites[1];
                        break;
                    case 3:
                        characterRenderer.sprite = idleSprites[2];
                        break;
                    case 4:
                        characterRenderer.sprite = idleSprites[3];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 5:
                        characterRenderer.sprite = idleSprites[3];
                        break;
                    case 6:
                        characterRenderer.sprite = idleSprites[4];
                        break;
                    case 7:
                        characterRenderer.sprite = idleSprites[4];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                }
                break;
            case 2:
                switch (currentDirection)
                {
                    case 0:
                        characterRenderer.sprite = idleSprites[2];
                        break;
                    case 1:
                        characterRenderer.sprite = idleSprites[1];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 2:
                        characterRenderer.sprite = idleSprites[0];
                        break;
                    case 3:
                        characterRenderer.sprite = idleSprites[1];
                        break;
                    case 4:
                        characterRenderer.sprite = idleSprites[4];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 5:
                        characterRenderer.sprite = idleSprites[3];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 6:
                        characterRenderer.sprite = idleSprites[3];
                        break;
                    case 7:
                        characterRenderer.sprite = idleSprites[4];
                        
                        break;
                }
                break;
            case 3:
                switch (currentDirection)
                {
                    case 0:
                        characterRenderer.sprite = idleSprites[1];
                        break;
                    case 1:
                        characterRenderer.sprite = idleSprites[2];
                        break;
                    case 2:
                        characterRenderer.sprite = idleSprites[1];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 3:
                        characterRenderer.sprite = idleSprites[0];
                        break;
                    case 4:
                        characterRenderer.sprite = idleSprites[4];
                        break;
                    case 5:
                        characterRenderer.sprite = idleSprites[4];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 6:
                        characterRenderer.sprite = idleSprites[3];
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;
                    case 7:
                        characterRenderer.sprite = idleSprites[3];

                        break;
                }
                break;

        }
    }

    private void LateUpdate()
    {
        //reset to NavMesh height
        NavMeshHit hit;
        if (NavMesh.SamplePosition(transform.position, out hit, 0.5f, NavMesh.AllAreas))
        {
            transform.position = hit.position;
        }
    }

    //calculate actual distance
    public float CalculateDistance(NodeSystems _system1, NodeSystems _system2)
    {
        //distance formula, converted from double to string to float
        float _nodeDistance = float.Parse(Math.Sqrt(Math.Pow(_system1.transform.position.x - _system2.transform.position.x, 2) +
                                                        Math.Pow(_system1.transform.position.y * 2 - _system2.transform.position.y * 2, 2) +
                                                        Math.Pow(_system1.transform.position.z - _system2.transform.position.z, 2)).ToString()
                                                    );
        return _nodeDistance;
    }

    //search parse list for specific system
    public NodeParse FindParse(List<NodeParse> _parses, NodeSystems _targetNode)
    {
        foreach (NodeParse _parse in _parses)
        {
            if (_parse.system == _targetNode) return _parse;
        }

        return null;
    }

    public IEnumerator FindPath(NodeSystems _targetNode)
    {

        //list to search for lowest F value.
        List<NodeParse> _nodesToCheck = new List<NodeParse>();

        //searched nodes are moved here
        List<NodeParse> _closedNodes = new List<NodeParse>();

        //parse our current node
        var _startParse = new NodeParse(currentNode, null, 0, CalculateDistance(currentNode, _targetNode));
        //add to list
        _nodesToCheck.Add(_startParse);
        //debug text
        currentNode.aText.text = $"{_startParse.f} -- {_startParse.g} + {_startParse.h}";

        //iteration counter to prevent frame lag
        int i = 0;
        while (_nodesToCheck.Count > 0)
        {
            //check first node in list
            NodeParse _selectedNode = _nodesToCheck[0];
            foreach (var _n in _nodesToCheck)
            {
                //unless this one h as a lower F or equal F with lower H
                if (_n.f < _selectedNode.f || (_n.f == _selectedNode.f && _n.h < _selectedNode.h))
                    _selectedNode = _n;
            }

            //if we've found the target node
            if (_selectedNode.system == _targetNode)
            {

                //move along path
                StartCoroutine(ExecuteParsePath(_selectedNode));

                //finish loop
                break;
            }

            //checked node removed and closed
            _nodesToCheck.Remove(_selectedNode);
            _closedNodes.Add(_selectedNode);

            //cardinal movement distance from our current node
            float _distance = _selectedNode.g + 1f;

            int j = 0;
            //search all connections (1 - 4 is cardinal, 5 - 8 is diagonal)
            foreach (Connector _connect in _selectedNode.system.GetConnectionsAsArray(_selectedNode.system.connections))
            {
                //diagonals if we've done it 4 times already, increase movement distance
                if (j >= 4) _distance = _selectedNode.g + 1.4f;

                //if the system connected is not null & has no character
                if (_connect.connection != null && !_connect.connection.currentCharacter)
                {
                    //check if we're looking to check this connection in the future
                    NodeParse _queuedParse = FindParse(_nodesToCheck, _connect.connection);
                    //check if we've already checked this connection
                    NodeParse _closedParse = FindParse(_closedNodes, _connect.connection);

                    //get real distance from the connection to our target
                    float _nodeDistance = CalculateDistance(_connect.connection, _targetNode);

                    //if the node hasn't been queued and hasn't been closed
                    if (_queuedParse == null && _closedParse == null)
                    {
                        //create a brand new parse class on the heap
                        var _p = new NodeParse(_connect.connection, _selectedNode, _distance, _nodeDistance);

                        //add it to be checked in the future
                        _nodesToCheck.Add(_p);

                        //debug text
                        _connect.connection.aText.text = $"{_p.f} -- {_p.g} + {_p.h}";
                    }
                    //if we're looking to check it in the future and the g distance to it is less than what was set up previously
                    else if (_queuedParse != null && _queuedParse.g > _distance)
                    {
                        //modify distance of the parse
                        _queuedParse.g = _distance;
                        //change the parse's closest connection to the node we are checking
                        _queuedParse.closestConnection = _selectedNode;

                        //debug text
                        _connect.connection.aText.text = $"{_queuedParse.f} -- {_queuedParse.g} + {_queuedParse.h}";
                    }
                }

                j++;

            }

            i++;

            //over maximum iterations, so wait until next frame
            if (i > 18)
            {
                yield return null;
                i = 0;
            }
        }
    }

    public IEnumerator ExecuteParsePath(NodeParse target)
    {
        var _agent = GetComponent<NavMeshAgent>();
        if (_agent) _agent.ResetPath();

        CombatCharacter.MovingCharacter = null;
        //input target parse and organize path into list
        List<NodeParse> _path = new List<NodeParse>();
        NodeParse _node = target;
        NodeSystems _previousNode = currentNode;
        while (_node.system != currentNode)
        {
            _path.Add(_node);
            _node = _node.closestConnection;
        }

        combat.moveRange -= target.g;

        //make camera follow this
        CameraTarget.Instance.following = transform;
        Vector3[] _pointPath = new Vector3[_path.Count];
        int[] _directionPath = new int[_path.Count];

        _directionPath[0] = GetDirectionFromNodes(currentNode,_path[_path.Count - 1].system);
        //iterate through in reverse to plot path
        for (int i = 1; i <= _path.Count; i++)
        {
            _node = _path[_path.Count - i];

            //add square entrance point
            _pointPath[i - 1] = _node.system.transform.position;

            //get direction between all the nodes and fill the array with them
            int _directionIndex = _path.Count - i - 1;
            if (_directionIndex >= 0 && i != _path.Count)
            {
                var _nextNode = _path[_directionIndex];
                _directionPath[i] = GetDirectionFromNodes(_node.system, _nextNode.system);
            }
            
        }


        //transfer character to target square
        currentNode.SetCharacter(null);
        target.system.SetCharacter(this);

        //move and "rotate" sprite on paths
        float _moveTime = 0.3f;
        if (_pointPath.Length > 1)
        {
            var _pathTime = _moveTime * _pointPath.Length;

            //move to target
            iTween.MoveTo(gameObject, iTween.Hash("path", _pointPath, "time", _pathTime, "easetype", iTween.EaseType.linear));

            for (int i = 0; i < _directionPath.Length; i++)
            {
                
                currentDirection = _directionPath[i];
                yield return new WaitForSeconds(_moveTime);
            }
        }
        else if (_pointPath.Length == 1)
        {
            //move to target
            iTween.MoveTo(gameObject, iTween.Hash("position", _pointPath[0], "time", _moveTime, "easetype", iTween.EaseType.linear));

            currentDirection = _directionPath[0];

            yield return new WaitForSeconds(_moveTime);
        }

        
        if (combat.moveRange >= 1 && combat.gameObject == MovementController.Instance.gameObject)
        {
            combat.ActivateMovement();
        }
        //change previous node for calculating move direction
        //_previousNode = _node.system;
    }


    //enable all nodes within movement range
    public IEnumerator EnableWalkRange(float distance)
    {
        Debug.Log($"Enabling a range of {distance}");
        float _g = 0;

        Queue<NodeParse> _searchQueue = new Queue<NodeParse>();
        List<NodeParse> _closed = new List<NodeParse>();
        List<NodeParse> _toEnable = new List<NodeParse>();

        //start with our current node
        _searchQueue.Enqueue(new NodeParse(currentNode, null, 0, 0));

        //while the queue has items
        while (_searchQueue.Count > 0)
        {
            var _selectedNode = _searchQueue.Dequeue();
            _g = _selectedNode.g + 1;
            //skip if the distance is too far.
            if (_selectedNode.g > distance) continue;

            _toEnable.Add(_selectedNode);

            int j = 0;
            //check all connections
            foreach (Connector _connect in _selectedNode.system.GetConnectionsAsArray(_selectedNode.system.connections))
            {
                if (j >= 4) _g = _selectedNode.g + 1.4f;


                if (_connect.connection != null && !_connect.connection.currentCharacter)
                {
                    NodeParse _closedParse = FindParse(_closed, _connect.connection);
                    //if we don't have the node already
                    if (_closedParse == null)
                    {
                        var _p = new NodeParse(_connect.connection, _selectedNode, _g, 0);

                        //add the node
                        _closed.Add(_p);
                        _searchQueue.Enqueue(_p);


                        _connect.connection.aText.text = $"{_p.f} -- {_p.g} + {_p.h}";
                    }
                    //or change its value to the new low.
                    else if (_closedParse.g > _g)
                    {
                        _closedParse.g = _g;
                        _closedParse.closestConnection = _selectedNode;

                        _connect.connection.aText.text = $"{_closedParse.f} -- {_closedParse.g} + {_closedParse.h}";
                    }
                }

                j++;

            }
        }

        Debug.Log($"Found {_toEnable.Count} square to enable.");
        foreach (NodeParse _parse in _toEnable)
        {
            _parse.system.nodeCanvas.SetActive(true);
            _parse.system.ChangeColor(Color.white);

            
        }

        currentNode.nodeCanvas.SetActive(false);

        yield return null;
    }

    //world space ability range
    public IEnumerator EnableSphereRange(float distance)
    {
        Vector3 _start = currentNode.rangePoint.position;

        //increase size to catch everything correctly
        float _range = distance + 0.5f;
        //grab all colliders within the sphere range
        Collider[] _hitNodes = Physics.OverlapSphere(_start, _range);


        //for each collider, check if we can raycast cleanly to it. If we can't there's an obstacle in the way
        foreach (Collider _collision in _hitNodes)
        {
            NodeSystems _system = _collision.GetComponent<NodeSystems>();
            if (_system)
            {
                RaycastHit _hit;
                if (Physics.Linecast(_start, _system.targetPoint.position, out _hit))
                {
                    if (_hit.collider == _collision)
                    {
                        _system.nodeCanvas.SetActive(true);
                        _system.ChangeColor(Color.red);
                    }
                }

            }
        }

        yield return null;
    }

    public int GetDirectionFromNodes(NodeSystems _start, NodeSystems _end)
    {
        int _x = 0;
        int _z = 0;

        if (_end.coordinate.x > _start.coordinate.x) _x = 1;
        else if (_end.coordinate.x < _start.coordinate.x) _x = -1;

        if (_end.coordinate.z > _start.coordinate.z) _z = 1;
        else if (_end.coordinate.z < _start.coordinate.z) _z = -1;

        if (_x > 0)
        {
            if (_z > 0)
            {
                return 6;
            }
            else if (_z < 0)
            {
                return 7;
            }
            else return 3;
        }
        else if (_x < 0)
        {
            if (_z > 0)
            {
                return 5;
            }
            else if (_z < 0)
            {
                return 4;
            }
            else return 1;
        }
        else
        {
            if (_z > 0)
            {
                return 2;
            }
            else if (_z < 0)
            {
                return 0;
            }
        }

        return currentDirection;
    }
}
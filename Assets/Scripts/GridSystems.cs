using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystems : MonoBehaviour
{
    public static GridSystems Instance;

    public GameObject gridPrefab;
    public NodeSystems[,,] gridNodes;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(gameObject);

        CreateGrid(25, 16, 25);
    }

    public NodeSystems GetSystemsFromPoint(Vector3 _vector)
    {
        return GetSystemsFromPoint(_vector.x, _vector.y, _vector.z);
    }

    //convert a world position to a point on the grid
    public NodeSystems GetSystemsFromPoint(float _x, float _y, float _z)
    {
        if (_x >= 0 && _x < gridNodes.GetLength(0) &&
            _y >= 0 && _y < gridNodes.GetLength(1) &&
            _z >= 0 && _z < gridNodes.GetLength(2))
        {
            var _xFloor = Mathf.FloorToInt(_x);
            var _yFloor = Mathf.RoundToInt(_y);
            var _zFloor = Mathf.FloorToInt(_z);
            Debug.Log($"Node Searched at: ({Mathf.FloorToInt(_xFloor)}, {_yFloor}, {_zFloor})");
            Debug.Log($"Node Found: ({gridNodes[_xFloor, _yFloor, _zFloor]})");
            return gridNodes[_xFloor, _yFloor, _zFloor];
        }
        else return null;
    }


    //create new grid
    public void CreateGrid(int _width, int _height, int _depth)
    {
        //if we already have a grid, clear out the old one

        if (gridNodes != null)
        {
            foreach (NodeSystems _obj in gridNodes)
            {
                Destroy(_obj.gameObject);
            }
        }

        gridNodes = new NodeSystems[_width, _height, _depth];


        for (int _x = 0; _x < _width; _x++)
        {
            for (int _y = 0; _y < _height; _y++)
            {
                for (int _z = 0; _z < _depth; _z++)
                {
                    //create node object
                    GameObject _newObj = Instantiate(gridPrefab, transform);
                    //set in position
                    _newObj.transform.localPosition = new Vector3(_x, (float)_y / 4, _z);
                    NodeSystems _node = _newObj.GetComponent<NodeSystems>();

                    //set coordinate
                    _node.coordinate = new Coordinate(_x, _y, _z);

                    //begin node setup systems
                    StartCoroutine(_node.NodeChecks());

                    //add to array
                    gridNodes[_x, _y, _z] = _node;
                }
            }
        }

        //after we've made the whole grid, get ready for cleanup.
        StartCoroutine(NodeCleanup());
    }

    //turn off everything. Not very efficient as it goes through the whole grid.
    public void DisableAllNodes()
    {
        foreach (NodeSystems _node in gridNodes)
        {
            if (_node != null) _node.nodeCanvas.gameObject.SetActive(false);
        }
    }

    public IEnumerator NodeCleanup()
    {
        //frame 0
        //wait until frame 5 then disable everything.
        yield return null;
        //frame 1
        yield return null;
        //frame 2
        yield return null;
        //frame 3
        yield return null;
        //frame 4
        yield return null;
        //frame 5
        DisableAllNodes();
    }
}
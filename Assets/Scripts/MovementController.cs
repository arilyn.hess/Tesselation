using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator animator;

    public bool combatLock = false;

    public static MovementController Instance;

    private GridCharacter character;

    private float moveSpeed = 0.35f;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else if (Instance != this) Destroy(gameObject);

        agent = GetComponent<NavMeshAgent>();
        character = GetComponent<GridCharacter>();

    }


    private void FixedUpdate()
    {
        
        if (combatLock)
        {
            if (agent.remainingDistance < 0.1f && agent.velocity == Vector3.zero)
            {
                agent.ResetPath();
            }
            
            return;
        }

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (input.magnitude == 0)
        {
            agent.ResetPath();
        }
        else
        {
            if (Mathf.Abs(input.y) > 0 || Mathf.Abs(input.x) > 0)
            {
                MoveCharacter(input);
            }
        }
    }

    private void MoveCharacter(Vector2 input)
    {
        Vector3 _destination = Vector3.zero;
        Vector3 xDistance = Vector3.zero;
        Vector3 zDistance = Vector3.zero;
        switch (CameraMovement.Instance.Rotation)
        {
            case 0:
                xDistance = Vector3.right * input.x * moveSpeed;
                zDistance = Vector3.forward * input.y * moveSpeed;
                break;
            case 1:
                xDistance = Vector3.right * input.y * moveSpeed;
                zDistance = Vector3.forward * -input.x * moveSpeed;
                break;
            case 2:
                xDistance = Vector3.right * -input.x * moveSpeed;
                zDistance = Vector3.forward * -input.y * moveSpeed;
                break;
            case 3:
                xDistance = Vector3.right * -input.y * moveSpeed;
                zDistance = Vector3.forward * input.x * moveSpeed;
                break;
        }

        if (xDistance.x > 0)
        {
            if (zDistance.z > 0)
            {
                character.currentDirection = 6;
            }
            else if (zDistance.z < 0)
            {
                character.currentDirection = 7;
            }
            else character.currentDirection = 3;
        }
        else if (xDistance.x < 0)
        {
            if (zDistance.z > 0)
            {
                character.currentDirection = 5;
            }
            else if (zDistance.z < 0)
            {
                character.currentDirection = 4;
            }
            else character.currentDirection = 1;
        }
        else
        {
            if (zDistance.z > 0)
            {
                character.currentDirection = 2;
            }
            else if (zDistance.z < 0)
            {
                character.currentDirection = 0;
            }
        }


        _destination = transform.position + xDistance + zDistance;

        agent.destination = _destination;

    }
}

using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public struct NodeConnections
{
   

    //up, down, left, right
    public Connector U;
    public Connector D;
    public Connector L;
    public Connector R;

    //diagonals
    public Connector UL;
    public Connector UR;
    public Connector DL;
    public Connector DR;

    public NodeConnections(Connector _up, Connector _down, Connector _left, Connector _right,
                           Connector _upLeft, Connector _upRight, Connector _downLeft, Connector _downRight)
    {
        U = _up;
        D = _down;
        L = _left;
        R = _right;
        UL = _upLeft;
        UR = _upRight;
        DL = _downLeft;
        DR = _downRight;
    }
}
public struct Connector
{
    public enum ConnectionType
    {
        NONE,
        BLOCKED
    }

    public NodeSystems connection;
    public ConnectionType type;

    public Connector(NodeSystems _connection, ConnectionType _type)
    {
        connection = _connection;
        type = _type;
    }

    public Connector(ConnectionType _type)
    {
        connection = null;
        type = _type;
    }
}

public struct Coordinate
{
    public int x;
    public int y;
    public int z;

    public Coordinate(int _x, int _y, int _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }
}

public class NodeSystems : MonoBehaviour
{

    public Coordinate coordinate;
    public NodeConnections connections;

    public GameObject nodeCanvas;
    public TextMeshProUGUI aText;

    public Image img;
    public Transform rangePoint;
    public Transform targetPoint;

    public GridCharacter currentCharacter;

    // Start is called before the first frame update
    void Awake()
    {
        nodeCanvas.GetComponent<Canvas>().worldCamera = Camera.main.GetComponentInChildren<Camera>();
        img = GetComponentInChildren<Image>();
    }

    private void Update()
    {
        foreach (Connector _connector in GetConnectionsAsArray(connections))
        {
            if (_connector.connection) Debug.DrawLine(transform.position, _connector.connection.transform.position, Color.yellow);
        }
    }

    public void SetCharacter(GridCharacter character)
    {
        currentCharacter = character;
        if (character) character.currentNode = this;
    }

    public Connector[] GetConnectionsAsArray(NodeConnections _connections)
    {
        var _array = new Connector[8];

        _array[0] = _connections.U;
        _array[1] = _connections.D;
        _array[2] = _connections.L;
        _array[3] = _connections.R;
        _array[4] = _connections.UL;
        _array[5] = _connections.UR;
        _array[6] = _connections.DL;
        _array[7] = _connections.DR;

        return _array;
    }

    public IEnumerator NodeChecks()
    {
        //frame 0
        nodeCanvas.SetActive(false);

        yield return null;
        //frame 1

        Connector _up = new Connector(Connector.ConnectionType.NONE);
        Connector _down = new Connector(Connector.ConnectionType.NONE);
        Connector _left = new Connector(Connector.ConnectionType.NONE);
        Connector _right = new Connector(Connector.ConnectionType.NONE);
        Connector _upLeft = new Connector(Connector.ConnectionType.NONE);
        Connector _upRight = new Connector(Connector.ConnectionType.NONE);
        Connector _downLeft = new Connector(Connector.ConnectionType.NONE);
        Connector _downRight = new Connector(Connector.ConnectionType.NONE);

        bool _leftValid = coordinate.x - 1 >= 0;
        bool _rightValid = coordinate.x + 1 < GridSystems.Instance.gridNodes.GetLength(0);

        if (coordinate.z + 1 < GridSystems.Instance.gridNodes.GetLength(2))
        {
            _up.connection = GridSystems.Instance.gridNodes[coordinate.x, coordinate.y, coordinate.z + 1];
            if (_leftValid) 
                    _upLeft.connection = GridSystems.Instance.gridNodes[coordinate.x - 1, coordinate.y, coordinate.z + 1];
            if (_rightValid)
                    _upRight.connection = GridSystems.Instance.gridNodes[coordinate.x + 1, coordinate.y, coordinate.z + 1];
        }

        if (coordinate.z - 1 >= 0)
        {
            _down.connection = GridSystems.Instance.gridNodes[coordinate.x, coordinate.y, coordinate.z - 1];
            if (_leftValid)
                _downLeft.connection = GridSystems.Instance.gridNodes[coordinate.x - 1, coordinate.y, coordinate.z - 1];
            if (_rightValid)
                _downRight.connection = GridSystems.Instance.
                    gridNodes[coordinate.x + 1, coordinate.y, coordinate.z - 1];
        }

        if (_leftValid)
            _left.connection = GridSystems.Instance.gridNodes[coordinate.x - 1, coordinate.y, coordinate.z];
        if (_rightValid)
            _right.connection = GridSystems.Instance.gridNodes[coordinate.x + 1, coordinate.y, coordinate.z];

        connections = new NodeConnections(_up, _down, _left, _right, _upLeft, _upRight, _downLeft, _downRight);

        yield return null;
        //frame 2

        yield return null;
        //frame 3

        yield return null;
        //frame 4


        if (!nodeCanvas.activeSelf) Destroy(gameObject);

        yield return null;
        //frame 5
        //double check diagonals
        if (!connections.U.connection || !connections.L.connection) connections.UL.connection = null;
        if (!connections.U.connection || !connections.R.connection) connections.UR.connection = null;
        if (!connections.D.connection || !connections.L.connection) connections.DL.connection = null;
        if (!connections.D.connection || !connections.R.connection) connections.DR.connection = null;

        yield return null;
    }

    public void ClickNode()
    {
        Debug.Log("Node Clicked at: " + coordinate.x.ToString() + ", " + coordinate.y.ToString() + ", " + coordinate.z.ToString());

        if (CombatCharacter.MovingCharacter)
        {
            StartCoroutine(MovementController.Instance.GetComponent<GridCharacter>().FindPath(this));

            GridSystems.Instance.DisableAllNodes();
        }
        
    }

    public void ChangeColor(Color _c)
    {
        img.color = _c;
    }

    public void ChangeColor(Color32 _c)
    {
        img.color = _c;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NotificationController : MonoBehaviour
{
    public static NotificationController Instance;

    public Image background;
    public TextMeshProUGUI text;

    private void Start()
    {
        if (Instance == null) Instance = this;
        else if (Instance != this) Destroy(gameObject);

        var _noColor = new Color(1, 1, 1, 0);
        background.color = _noColor;
        text.color = _noColor;
    }

    public IEnumerator DisplayNotification(string displayText)
    {
        float _fadeTime = 0.5f;
        float _holdTime = 1.5f;

        float _timer = 0f;

        text.text = displayText;

        while (_timer < _fadeTime)
        {
            var _newColor =  new Color(1, 1, 1, _timer / _fadeTime);
            background.color = _newColor;
            text.color = _newColor;

            _timer += Time.deltaTime;
            yield return null;
        }

        var _opaque = new Color(1, 1, 1, 1);
        background.color = _opaque;
        text.color = _opaque;

        yield return new WaitForSeconds(_holdTime);

        _timer = 0;

        while (_timer < _fadeTime)
        {
            var _newColor = new Color(1, 1, 1, 1 - (_timer / _fadeTime));
            background.color = _newColor;
            text.color = _newColor;

            _timer += Time.deltaTime;
            yield return null;
        }

        var _transparent = new Color(1, 1, 1, 0);
        background.color = _transparent;
        text.color = _transparent;
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSystems : MonoBehaviour
{
    public enum TileModes
    {
        STANDARD,
        RAMP
    }

    public TileModes mode;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ModifyNodeFromTile());
    }

    private IEnumerator ModifyNodeFromTile()
    {
        //frame 0
        yield return null;
        //frame 1


        yield return null;
        //frame 2
        switch (mode)
        {

            case TileModes.RAMP:
                NodeSystems _rampNode = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                    transform.position.y * 4 - 1,
                                                                                    transform.position.z);
                if (_rampNode)
                {
                    _rampNode.nodeCanvas.SetActive(true);
                    var _childTransform = GetComponentInChildren<Renderer>().transform;

                    Debug.Log("Y Rotation: " + _childTransform.eulerAngles.y.ToString());
                    if (_childTransform.eulerAngles.y == 0)
                    {
                        _rampNode.transform.eulerAngles = new Vector3(-27, 0, 0);

                        NodeSystems _backNodeDown = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                                transform.position.y * 4 - 2,
                                                                                                transform.position.z - 1);

                        if (_backNodeDown)
                        {
                            _backNodeDown.connections.U.connection = _rampNode;
                            _backNodeDown.connections.UL.connection = null;
                            _backNodeDown.connections.UR.connection = null;
                            _rampNode.connections.D.connection = _backNodeDown;
                        }

                        NodeSystems _frontNodeUp = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                                transform.position.y * 4,
                                                                                                transform.position.z + 1);

                        if (_frontNodeUp)
                        {
                            _frontNodeUp.connections.D.connection = _rampNode;
                            _frontNodeUp.connections.DL.connection = null;
                            _frontNodeUp.connections.DR.connection = null;
                            _rampNode.connections.U.connection = _frontNodeUp;
                        }
                    }
                    else if (_childTransform.eulerAngles.y == 90)
                    {
                        _rampNode.transform.eulerAngles = new Vector3(0, 0, 27);

                        NodeSystems _backNodeLeft = GridSystems.Instance.GetSystemsFromPoint(transform.position.x - 1,
                                                                                                transform.position.y * 4 - 2,
                                                                                                transform.position.z);

                        if (_backNodeLeft)
                        {
                            _backNodeLeft.connections.R.connection = _rampNode;
                            _backNodeLeft.connections.UR.connection = null;
                            _backNodeLeft.connections.DR.connection = null;
                            _rampNode.connections.L.connection = _backNodeLeft;
                        }

                        NodeSystems _frontNodeRight = GridSystems.Instance.GetSystemsFromPoint(transform.position.x + 1,
                                                                                                transform.position.y * 4,
                                                                                                transform.position.z);

                        if (_frontNodeRight)
                        {
                            _frontNodeRight.connections.L.connection = _rampNode;
                            _frontNodeRight.connections.UL.connection = null;
                            _frontNodeRight.connections.DL.connection = null;
                            _rampNode.connections.R.connection = _frontNodeRight;
                        }
                    }
                    else if (_childTransform.eulerAngles.y == 180)
                    {
                        _rampNode.transform.eulerAngles = new Vector3(27, 0, 0);

                        NodeSystems _backNodeUp = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                                transform.position.y * 4,
                                                                                                transform.position.z - 1);

                        if (_backNodeUp)
                        {
                            _backNodeUp.connections.U.connection = _rampNode;
                            _backNodeUp.connections.UL.connection = null;
                            _backNodeUp.connections.UR.connection = null;
                            _rampNode.connections.D.connection = _backNodeUp;
                        }

                        NodeSystems _frontNodeDown = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                                transform.position.y * 4 - 2,
                                                                                                transform.position.z + 1);

                        if (_frontNodeDown)
                        {
                            _frontNodeDown.connections.D.connection = _rampNode;
                            _frontNodeDown.connections.DL.connection = null;
                            _frontNodeDown.connections.DR.connection = null;
                            _rampNode.connections.U.connection = _frontNodeDown;
                        }
                    }
                    else if (_childTransform.eulerAngles.y == 270)
                    {
                        _rampNode.transform.eulerAngles = new Vector3(0, 0, -27);

                        NodeSystems _backNodeRight = GridSystems.Instance.GetSystemsFromPoint(transform.position.x + 1,
                                                                                                transform.position.y * 4 - 2,
                                                                                                transform.position.z);

                        if (_backNodeRight)
                        {
                            _backNodeRight.connections.L.connection = _rampNode;
                            _backNodeRight.connections.UL.connection = null;
                            _backNodeRight.connections.DL.connection = null;
                            _rampNode.connections.R.connection = _backNodeRight;
                        }

                        NodeSystems _frontNodeLeft = GridSystems.Instance.GetSystemsFromPoint(transform.position.x - 1,
                                                                                                transform.position.y * 4,
                                                                                                transform.position.z);

                        if (_frontNodeLeft)
                        {
                            _frontNodeLeft.connections.R.connection = _rampNode;
                            _frontNodeLeft.connections.UR.connection = null;
                            _frontNodeLeft.connections.DR.connection = null;
                            _rampNode.connections.L.connection = _frontNodeLeft;
                        }
                    }
                }
                break;
            default:


                NodeSystems _node = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                    transform.position.y * 4,
                                                                                    transform.position.z);

                if (_node)
                {
                    _node.nodeCanvas.SetActive(true);
                }

                break;
        }

        yield return null;

        //frame 3

        if (transform.position.y * 4 > 1)
        {
            NodeSystems _underNode1 = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                transform.position.y * 4 - 1,
                                                                                transform.position.z);
            if (_underNode1 && mode != TileModes.RAMP) _underNode1.nodeCanvas.SetActive(false);
            else Debug.Log("Under1 Null");

            NodeSystems _underNode2 = GridSystems.Instance.GetSystemsFromPoint(transform.position.x,
                                                                                transform.position.y * 4 - 2,
                                                                                transform.position.z);
            if (_underNode2) _underNode2.nodeCanvas.SetActive(false);
            else Debug.Log("Under2 Null");
        }
    }
}
